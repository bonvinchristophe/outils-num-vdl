# Des outils pour la projection en classe :computer:

### :one: Digiscreen : Gestion de classe
**Digiscreen** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Il permet de créer un tableau pour la gestion de classe avec différents outils : chronomètre, minuteur, documents à projeter, tirage au sort, générateur de groupe, générateur de QR Code, etc.

Il est possible de faire ses "screens" à l'avance, de les enregistrer sur un support (clé USB, espace de travail) et de les ouvrir par la suite.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digiscreen/](https://ladigitale.dev/digiscreen/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiscreen](https://ladigitale.dev/digiplay/#/v/634d72dc14c81){:target="_blank" }
    - Lien vers un [fichier test à télécharger](https://nuage03.apps.education.fr/index.php/s/6QGMjgmqJiLtyfd){:target="_blank" } et à importer dans Digiscreen

### :two: Tableau en sciences

**tableau.ensciences.fr** est un outil similaire à Digiscreen mais fournit un peu moins de fonctionnalités que Digiscreen. Il possède les fonctionnales de base avec quelques autres plus poussées pour les sciences : Editeur d'équations, Editeur de molécules, GeoGebra, Console Python et Calculatrice (Ti83 et Numworks)


!!! info "Remarque"
    Pour l'instant il ne semble pas possible d'exporter et d'enregistrer ses "screens". L'auteur est en train d'y travailler.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://tableau.ensciences.fr/](https://tableau.ensciences.fr/){:target="_blank" }
    


### :three: OpenBoard : Tableau blanc interactif
**OpenBoard** est un logiciel installé sur tous les PC profs du lycée. Il permet de créer des diapositives avec des documents numériques (vidéos, images, textes, etc.) et de les annoter avec les stylets du VPI. Il propose des outils d’annotation, de dessin, de géométrie pour réaliser vos figures et votre trace écrite. Les diapositives peuvent être préparées à l’avance (en amont de la séance). Une fois le cours réalisé, le tout peut être exporté au format PDF pour être joint au cahier de texte par exemple.

!!! tip "Prise en main"
    - Lien vers l’outil : [https://openboard.ch/download.html](https://openboard.ch/download.html){:target="_blank" }
    - Lien vers la documentation : [toutes les fonctionnalités d’OpenBoard](https://openboard.ch/download/Tutoriel_OpenBoard_1.6.pdf){:target="_blank" }
    - Lien vers la documentation simplifiée : [Partie 1](https://openboard.ch/download/OpenBoard1.6_interface.pdf){:target="_blank" } – [Partie 2](https://openboard.ch/download/OpenBoard1.6_Mode_Doc_en_1_page.pdf){:target="_blank" }
    - Lien vers un tutoriel vidéo : [Prise en main](https://ladigitale.dev/digiplay/#/v/63568906a8b20){:target="_blank" } – [Les outils en géométrie](https://ladigitale.dev/digiplay/#/v/63568b1938a27){:target="_blank" } – [Les applications dans OpenBoard](https://ladigitale.dev/digiplay/#/v/63568b65100fb){:target="_blank" } – [Annoter une document PDF](https://ladigitale.dev/digiplay/#/v/63568bd397b03){:target="_blank" } – [Exporter sa présentation en pdf](https://ladigitale.dev/digiplay/#/v/63568c0f58bb3){:target="_blank" } – [Bien utiliser OpenBoard](https://ladigitale.dev/digiplay/#/v/63568e1c83ce5){:target="_blank" } – [Gérer ses classes et ses documents](https://ladigitale.dev/digiplay/#/v/63568e7665c73){:target="_blank" } – [Annoter un tableau](https://ladigitale.dev/digiplay/#/v/63568ea3f24a6){:target="_blank" }

### :four: Cool Maze : Transférer un média (photo ou vidéo) directement de votre smartphone vers l’écran
**Cool Maze** est une application pour smartphone qui vous permet en moins de 10s d’afficher une photo (ou une vidéo) de votre téléphone sur votre ordinateur (et donc sur le vidéoprojecteur qui lui est relié). 

_Possibilités_ : 
- afficher un travail d’élève au tableau
- afficher une page de livre ou d’un document au tableau
- demander aux élèves de prendre une photo d’une expérience, ils peuvent ensuite la télécharger sur leur PC et la travailler

!!! tip "Prise en main"
    - Lien vers l’outil : [https://coolmaze.io/](https://coolmaze.io/){:target="_blank" }
    - Lien vers l’appli (Android) : [https://play.google.com/store/apps/details?id=com.bartalog.coolmaze](https://play.google.com/store/apps/details?id=com.bartalog.coolmaze){:target="_blank" }
    - Lien vers l’appli (iOS) : [https://apps.apple.com/us/app/cool-maze/id1284597516](https://apps.apple.com/us/app/cool-maze/id1284597516){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Cool Maze](http://lnb.svt.free.fr/nouveau lycee/-document/FM/FT L27 Tuto-Cool-Maze.pdf){:target="_blank" }


