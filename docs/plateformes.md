# Des outils pour créer des séquences d'apprentissage :notebook: 

### :one: DigiSteps : Créer des parcours pédagogiques
**Digisteps** a pour objectif de répondre à un besoin simple : offrir aux formateurs et aux enseignants qui n'ont pas accès à une plateforme de gestion des apprentissages (LMS) la possibilité de créer des parcours pédagogiques simples à partager avec les apprenants. L'outil permet ainsi d'agréger, au sein d'une même page, différents ingrédients pédagogiques :
    • des séances en présence ou à distance ;
    • des documents (fichiers et liens) ;
    • des exercices (fichiers et liens) ;
    • des activités (fichiers et liens, avec ou sans évaluation).
Les parcours générés avec Digisteps peuvent servir de fil conducteur lors d'une formation ou d'une séquence pédagogique. En activant l'option de dépôt pour les activités, les apprenants peuvent également transmettre leurs travaux, sans créer de compte. Les étapes sont visibles par défaut par tous les apprenants, mais il est possible de les masquer et de les rendre disponibles au fur et à mesure de l'avancée dans le parcours.
Un système de code d'accès permet également de masquer le contenu d'une étape, ce qui permet, par exemple, d'utiliser Digisteps pour des parcours ludiques, de type chasse au trésor, jeu de piste, jeu d'évasion, etc.

 
!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digisteps/](https://ladigitale.dev/digisteps/){:target="_blank" }
    - Lien vers le tutoriel : [Vidéo 1](https://ladigitale.dev/digiplay/#/v/6357dc9d4d2b7){:target="_blank" } - [Vidéo 2](https://ladigitale.dev/digiplay/#/v/6357dc50dbe65){:target="_blank" }


### :two: Elea : une plateforme d'apprentissage basée sur Moodle et comparable à Google Classroom :new:
La plateforme Moodle **Éléa** permet aux enseignants de créer des parcours pédagogiques scénarisés pour les élèves afin de mettre en œuvre certains principes de l’enseignement hybride comme la pédagogie inversée par exemple. Les élèves peuvent progresser à leur rythme grâce aux contenus pédagogiques proposés en ligne par leur enseignant.

Afin de favoriser l’engagement et la motivation des élèves pour réaliser le travail proposé en ligne, la plateforme Éléa offre la possibilité d’intégrer la gamification aux situations d’apprentissage, en reprenant des mécanismes empruntés aux jeux vidéos (carte de progression, défis, obtention de récompenses ou de badges, ...).

La mutualisation étant un concept essentiel en matière de pédagogie, des parcours-ressources validés par les corps d’inspection sont mis à disposition des enseignants ayant envie de découvrir les fonctionnalités de la plateforme. Ils peuvent les consulter dans la Éléathèque, se les approprier et les adapter en fonction de leurs besoins.

Pour accéder à la plateforme Éléa, les élèves se connectent à leur ENT. Une fois authentifiés, ils arrivent sur leur espace où sont listés leurs dossiers et les parcours auxquels ils sont inscrits.
Les enseignants disposent de droits supplémentaires. Ils peuvent créer leur premier parcours en utilisant les gabarits pré-construits. Ils ont également accès à la Éléathèque et aux tutoriels si besoin.
L’interface a été simplifiée et pensée pour faciliter l’utilisation de la plateforme. Un travail sur l’ergonomie a été réalisé afin de rendre la prise en main de l’outil la plus intuitive possible.

!!! tip "Prise en main"
    - Lien vers l’outil : Accéder à l'application **ELEA** depuis l'ENT
    - Lien vers le tutoriel : Une [page](https://ressources.dane.ac-versailles.fr/spip.php?page=tutoriel&id_ressource=126){:target="_blank" } avec différents tutoriels pour la prise en main et la conception des activités

