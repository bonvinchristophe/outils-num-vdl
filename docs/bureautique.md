# Des outils pour créer des documents numériques, les partager :floppy_disk:

### :one: Pack Office 365 : Suite Bureautique 
C’est la suite bureautique de Microsoft. Une licence est offerte par la région pour chaque enseignant et chaque élève au lycée. Cette suite regroupe différents outils comme un traitement de texte (Word), un tableur (Excel), un logiciel de présentation (PowerPoint), un logiciel de messagerie (Outlook), un espace de stockage de 1To (OneDrive), et d’autres outils. Il est possible d’utiliser ces outils en ligne ou de les installer sur plusieurs appareils (5 PC ou Mac, 5 tablettes et 5 smartphones)


!!! tip "Prise en main"
    - Lien vers l’outil : [https://www.office.com/](https://www.office.com/){:target="_blank" }
    
    
!!! info "Remarque"
    - Il vous faudra vos identifiants fournis par la région pour accéder au site
    - Pour votre identifiant sur office, il faudra ajouter @eduhdf.fr, exemple : mdupont@eduhdf.fr
    - Si vous n'avez pas reçu de codes, contacter le support : support365@eduhdf.fr


### :two: Libre Office : Suite Bureautique

C’est une suite bureautique à Office 365 mais celle-ci est libre et gratuite. On y retrouve les mêmes outils exceptés l’espace de stockage.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://fr.libreoffice.org/download/telecharger-libreoffice/](https://fr.libreoffice.org/download/telecharger-libreoffice/){:target="_blank" }

### :three: Réaliser des documents collaboratifs simples (traitement de texte, tableur, image)
#### :arrow_forward: Digidoc : Traitement de texte collaboratif
**Digidoc** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Basé sur le logiciel libre Etherpad Lite, Digidoc permet de créer des documents collaboratifs. 

_Voici des exemples d'usages pédagogiques_ : rédaction d'une histoire à plusieurs mains, développement d'un schéma narratif, débat d'idées et fil de discussion thématique, etc. Les documents peuvent être créés sans compte et par un simple clic. Il est important de récupérer immédiatement le lien vers le document (en copiant le lien dans la barre d'adresse du navigateur par exemple). L'outil propose des options de formatage pour le texte et offre la possibilité d'insérer des liens et de commenter les contributions (pour une évaluation entre pairs ou une rétroaction de l'enseignant par exemple). Les contributions des participants apparaissent en temps réel sur le document et peuvent être facilement identifiées grâce à un système de couleurs personnalisables.

!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digidoc/](https://ladigitale.dev/digidoc/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digidoc](https://ladigitale.dev/digiplay/#/v/6356c3535bafb){:target="_blank" }


#### :arrow_forward: Digicalc : Tableur collaboratif
**Digicalc** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Basé sur le logiciel libre Ethercalc, Digicalc permet de créer des feuilles de calcul collaboratives pour la mise en place d'activités et de projets pédagogiques, la collaboration entre pairs, etc.

_Exemples d'usages_ : sitographie collaborative pour répertorier des liens entre pairs, fiche de lecture, gestion de projet, comptage en classe avec plusieurs groupes, etc.

Les documents peuvent être créés sans compte et par un simple clic. Il est important de récupérer immédiatement le lien en le copiant depuis la barre d'adresse du navigateur et de le prendre en note afin de retrouver l'accès au document par la suite.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicalc/](https://ladigitale.dev/digicalc/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digidoc](https://ladigitale.dev/digiview/#/v/67141a5fac99f){:target="_blank" } à partir de 7min33

#### :arrow_forward: Digiboard : Un tableau blanc collaboratif
**Digiboard** est une application libre et gratuite de la plateforme Ladigitale.dev créée par Emmanuel Zimmert.

Elle permet de créer des tableaux blancs collaboratifs pour :
    • annoter ou commenter une image ;
    • faire un remue-méninge ;
    • créer des activités interactives (texte à trous, classement, glisser-déposer, etc.) ;
    • etc



!!! tip "Prise en main"
    - Lien vers l’outil : [https://digiboard.app/	](https://digiboard.app/	){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiboard](https://ladigitale.dev/digiplay/#/v/6357d4fb2ae6a){:target="_blank" } 
    

### :four: Digicode : Partager un lien avec un QR Code
Le site la digitale regroupe un ensemble d’outils dont deux qui permettent de communiquer plus simplement des liens en créant un QR code à partir d'une adresse internet (url). La couleur peut être paramétrée. On peut ajouter une image ou un logo pour personnaliser le QR code. L’outil se nomme **Digicode**



!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicode/ ](https://ladigitale.dev/digicode/ ){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digicode](https://ladigitale.dev/digiview/#/v/63569ee06697f){:target="_blank" }
    
    
!!! info "Remarque"
    - L'outil digilink qui permet de raccourcir les liens n'est plus disponible sur la digitale (sauf en souscrivant à Digidrive pour 25€/an)


### :five: LSTU : Raccourcir des liens
Il est parfois nécessaire de communiquer un lien vers une adresse web aux élèves mais la plupart du temps, ce dernier est très long et ne dit pas grand chose sur ce qu'on va trouver. Avec **LSTU**, non seulement vous pouuvez raccourcir les liens mais également leur donner un nom plus évocateur

!!! tip "Prise en main"
    - Lien vers l’outil : [https://lstu.fr/ ](https://lstu.fr/ ){:target="_blank" }
    - Exemple: [https://lstu.fr/vdl](https://lstu.fr/vdl){:target="_blank" }
    
    
### :six: DigiPDF : Modifier, convertir des pdf
**DigiPDF** regroupe une série de 52 outils pour travailler sur les fichiers PDF.

Parmi les modules proposés, vous pourrez :

    - convertir un fichier PDF en .odt, .docx, .odp, .pptx, .ods, .xlsx ;
    - réduire la taille d'un fichier trop volumineux ;
    - réorganiser, extraire ou fusionner des pages d'un fichier ;
    - ajouter ou supprimer un mot de passe à un document ;
    - etc

En ce qui concerne le traitement des fichiers téléversés, il y a 2 cas de figure : soit les fichiers restent du côté client (dans le navigateur), soit ils sont stockés temporairement sur le serveur durant l'exécution des tâches et supprimés par la suite (le dossier /tmp du serveur est vidé quotidiennement).

!!! tip "Prise en main"
    - Lien vers l’outil : [https://digipdf.app/ ](https://digipdf.app/ ){:target="_blank" }
    - Lien vers le tutoriel : [https://ladigitale.dev/digiview/#/v/6714277c9be89](https://ladigitale.dev/digiview/#/v/6714277c9be89){:target="_blank" }
    
    
### :seven: Canva : Créer des infographies, des diaporamas
**Canva** est un outil est plutôt destiné aux enseignants et permet de créer des infographies pour le cours, des affiches, etc. Il propose de nombreux modèles avec une banque d’image qui est riche. 



!!! tip "Prise en main"
    - Lien vers l’outil : [https://www.canva.com/fr_fr/ ](https://www.canva.com/fr_fr/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Canva](https://ladigitale.dev/digiplay/#/v/6356c9df767a4){:target="_blank" }
    
    
!!! info "Remarque"
    -Il y a une version gratuite et une payante (Premium). S’inscrire avec son adresse académique permet d’avoir accès à davantage de ressources que la version gartuite mais sans pour autant avoir toutes les fonctionnalités de la version Premium.

### :eight: Genially : Créer des diaporamas interactifs
**Genially** est une application en ligne qui permet de créer des contenus interactifs, en intégrant des images, des vidéos, des objets numériques et du texte, etc. Les usages de Genially en contexte pédagogique sont nombreux et n’ont pour limite que votre créativité ! Toutefois la prise en main peut nécessiter un certain temps.
Exemples : Créer des supports de cours sous forme de dossiers interactifs et les rendre disponibles aux élèves, créer des cartes interactives en géographie, réaliser un escape game, faire une ligne du temps interactive, créer un quizz pour réviser les différents sujets abordés.
Genially propose une version gratuite qui est suffisante pour travailler. Une version premium est disponible qui permet de ranger ses créations dans des dossiers, ou d’exporter son travail en PDF, png, site web ou vidéo.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://genially.com/fr/](https://genially.com/fr/){:target="_blank" }
    - Lien vers le tutoriel : [Présentation](https://profpower.lelivrescolaire.fr/tutoriel-creez-une-presentation-avec-genially/){:target="_blank" } - [Vidéo sur les bases 1/2](https://ladigitale.dev/digiplay/#/v/63579dda6c533){:target="_blank" } - [Vidéo sur les bases (animations) 2/2](https://ladigitale.dev/digiplay/#/v/63579dfa83bd1){:target="_blank" }
    
    
!!! info "Remarque"
    -Il existe une version Education gratuite en s'inscrivant avec son mail académique. Elle propose plus de modèles que la version payante mais les options d'exportation sont limitées comme dans la version gratuite (pas d'exportation en pdf, ni png, ni vidéo, ni HTML)

### :nine: Digicard : Modifier rapidement des images en ligne
**Digicard** est une application libre et gratuite de la plateforme **Ladigitale.dev** (créée par Emmanuel Zimmert). Ce module est un petit outil sans prétention pour créer des cartes virtuelles et des compositions graphiques simples à l’aide :
• de texte ;
• d’éléments graphiques (des formes, des bulles, etc.) ;
• de vos images personnelles.
Les paramètres de la composition sont automatiquement enregistrés dans le stockage local de votre navigateur.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicard/#/](https://ladigitale.dev/digicard/#/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digicard](https://ladigitale.dev/digiplay/#/v/6357d7e442e3d){:target="_blank" }
    
    
