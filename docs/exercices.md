# Des outils pour créer des exercices :page_facing_up:

### :one: QCM Pronote : Tester les élèves et les évaluer en même temps
**Pronote** permet de créer des QCM à réaliser aux élèves. Il peut calculer automatiquement une note qui peut, ou non, compter dans la moyenne de l’élève. 

Différentes options sont disponibles : mettre un temps pour le QCM ou une question, mélanger les questions et les réponses, revenir en arrière ou pas, mettre des images, une vidéo ou un son dans la question.

Les QCM élaborés peuvent être partagés avec les collègues de l’établissement



!!! tip "Prise en main"
    - Lien vers le site Index éducation. Sur cette page se trouvent de multiples tutoriels vidéo : [https://www.index-education.com/fr/tutoriels-video-pronote-686-29-utiliser-des-qcm-pour-evaluer-des-competences-espace-professeurs.php](https://www.index-education.com/fr/tutoriels-video-pronote-686-29-utiliser-des-qcm-pour-evaluer-des-competences-espace-professeurs.php){:target="_blank" }

### :two: Application “Exercice” de l’ENT : Créer des exercices à rendre

L’application **Exercice** permet de réaliser des questionnaires à choix multiples comme Pronote. Un barème peut être donné et une note est alors attribuée.

Il est également possible de créer un exercice à rendre. L’outil permet de créer une consigne, et l’élève peut la consulter dans son application Exercice. Il y a un bouton sur sa page pour joindre son travail.

Pour les enseignants, il y a une interface qui fait le bilan des élèves qui ont rendu ou non le travail.


!!! tip "Prise en main"
    - Vidéos tutoriel : 
        - [Partie 1](https://drive.google.com/file/d/19OOaenSZlaVXSopeJdKZC74jdNCemH9v/view?usp=sharing){:target="_blank" } 
        – [Partie 2](https://drive.google.com/file/d/19OOaenSZlaVXSopeJdKZC74jdNCemH9v/view?usp=sharing){:target="_blank" }

### :three: AMC : AutoMultipleChoice : Créer des QCM individualisés
Ce logiciel est réservé aux utilisateurs.rices avancés.ées car il nécessite de maîtriser le langage Latex et d’avoir un PC sous Linux (ou une clé USB bootable sous linux). C’est un logiciel de création de devoir qui permet surtout de réaliser des QCM (avec 1 ou plusieurs réponses), mais il est également possible de créer des questions ouvertes.

Ses fonctionnalités permettent, à partir d’une banque de questions que vous avez constituée, de générer un devoir différent pour chaque élève.

Il est possible de paramétrer le logiciel pour corriger automatiquement les questions (cas du QCM) et de calculer automatiquement la note (dans le cas du QCM et de questions ouvertes).
 
!!! tip "Prise en main"
    - Lien vers le site : [https://www.auto-multiple-choice.net/index.fr](https://www.auto-multiple-choice.net/index.fr){:target="_blank" }
    - Présentation succincte de la création d’un QCM (15min) : [https://ladigitale.dev/digiplay/#/v/635269f66810e](https://ladigitale.dev/digiplay/#/v/635269f66810e){:target="_blank" }
    - Lien tutoriel : [Partie 1](https://ladigitale.dev/digiplay/#/v/63526ac50346a){:target="_blank" } – [Partie 2](https://www.youtube.com/watch?v=gSVLQg-ZAQA){:target="_blank" } – [Partie 3](https://ladigitale.dev/digiplay/#/v/63526af918919){:target="_blank" }


### :four: Digiquiz et Logiquiz : Créer des exercices interactifs
**H5P** (pour HTML5 Package) est une solution simple pour créer et utiliser des outils interactifs de type exerciseur en ligne. La plateforme la digitale vous propose deux outils complémentaires pour créer vos contenus.
 
!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/](https://ladigitale.dev/){:target="_blank" }
    - Lien vers le tutoriel : [Installer et utiliser Logiquiz](https://ladigitale.dev/digiplay/#/v/6357daa1636fb){:target="_blank" }

