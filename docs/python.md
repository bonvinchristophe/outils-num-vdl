# Des outils pour travailler la programmation :snake:

### :one: Basthon : Travailler la programmation python en ligne 
Le site **Basthon.fr** permet de travailler le langage de programmation Python sans avoir besoin d’installer un interpréteur (ex : Edupython). Tout se passe en ligne sur le navigateur Web (Chrome ou Firefox conseillés). Il y a une grande quantité de modules déjà installés : numpy, matplotlib, maths, etc. On peut y joindre des documents pour y appliquer des opérations avec Python, on peut partager une fenêtre avec un code par lien (voir l’exemple ci-dessous). Il est possible de travailler directement depuis la console Python ou d’y faire des notebooks.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://basthon.fr/](https://basthon.fr/){:target="_blank" } - [Version Console](https://console.basthon.fr/):target="_blank" } - [Version Notebook](https://notebook.basthon.fr/):target="_blank" }
    - Lien vers le tutoriel : [Documentation](https://basthon.fr/doc.html){:target="_blank" }
    - Lien vers un [exemple](https://console.basthon.fr/?script=eJxLyy9SyFTIzFMoSsxLT1XQMDTQtOLlUgCCgqLMvBIFjUxNALfqCZ8){:target="_blank" }


### :two: Capytale : Travailler et évaluer la programmation
**Capytale** est un outil développé par des enseignants de l’académie de Paris et d’Orléans (ce sont les mêmes collègues qui ont développé Basthon). Il est accessible pour les enseignants et les élèves dans l’ENT. 
Il permet de réaliser plusieurs types d’activité :
- un script Console en python
- un notebook en python
- créer des jeux en python
- du pixel art
- du bloc (comme Scratch)
- travailler le langage SQL et OCAMl (programme de NSI)
- travailler du HTML, CSS et JavaScript (programme de SNT)
- travailler sur des cartes électroniques (microbit, arduino, etc.)
- travailler avec des robots (Mbot, thymio, etc.)

Les enseignants peuvent créer des activités et les partager par code aux élèves. Les élèves rendent les copies numériques en fin de séance et l’enseignant récupère automatiquement les copies avec le nom des élèves.



!!! info "Remarque"
    L'accès à Capytale se fait via l'ENT. Il suffit de cliquer sur le connecteur. 
    Il existe dans Capytale, une bibliothèque d’activités créées par des collègues et peuvent servir de base.


!!! tip "Prise en main"
    - Lien vers la documentation : [Utiliser Capytale](https://nuage03.apps.education.fr/index.php/s/j8E6mWTkZT2Lm2S){:target="_blank" }


### :three: Py-Rates : Apprendre l'algorithmique et la programmation Python par le jeu
C’est un jeu développé dans le cadre d'une thèse en didactique de l'informatique. Il s'agit d'un site Internet accessible via un navigateur web et qui permet une première approche de la programmation en Python pour les élèves de seconde sous la forme d'un jeu de plateforme sur le thème des pirates. L'application est simple d'usage (page unique) et ne nécessite aucune inscription (conforme au RGPD).



!!! tip "Prise en main"
    - Lien vers l’outil : [https://py-rates.fr/](https://py-rates.fr/){:target="_blank" }
    - Lien vers la documentation : [https://py-rates.fr/guide/FR/index.html](https://py-rates.fr/guide/FR/index.html){:target="_blank" }

!!! info "Remarque"
    Les solutions sont disponibles sur YouTube. Il faut bloquer l'accès au site pour éviter que les élèves ne les trouvent.
    


