# Des outils pour collaborer entre collègues :two_men_holding_hands: 

### :one: Espace documentaire : Stocker et partager des documents
L’application **espace documentaire** est une application accessible sur l’ENT pour les élèves comme les enseignants. Elle permet comme un « cloud » de stocker des fichiers personnels dans des dossiers (capacité de stockage de 4Go). Il est possible de créer un dossier et de le partager avec des collègues et/ou des élèves pour partager des documents


!!! tip "Prise en main"
    - Lien vers le tutoriel vidéo : [Présentation de l'espace documentaire](https://drive.google.com/file/d/1o8vNX3s4zcPn7XGfXbdHpQaKkbFb8BqG/view?usp=sharing){:target="_blank" }

### :two: Nuage d’Apps Education : Stocker et partager des documents

**Nuage** est une application comme Google Drive ou Dropbox, mais ici l’outil est hébergé par l’Education Nationale et vous propose un espace de stockage jusque 100Go. Pour y accéder il faut un compte sur Apps Education. 

Il permet de créer des documents en ligne (traitement de texte, tableur, diaporama) et de les partager avec des collègues ou des élèves. Il y a des règles qui permettent de donner le droit en lecture, et/ou en écriture (modification) sur les fichiers, de mettre une date d'expiration, un mot de passe, etc.

Ce service se revèle très pratique pour créer un espace de dépôt de travaux pour les élèves sans que ces derniers ne peuvent consulter ceux des camarades. (voir l'exemple ci-dessous)

!!! info "Exemple : Espace de dépôt"
    - [Lien à communiquer](https://nuage03.apps.education.fr/index.php/s/xXHpNrQ7BCRNiM7){:target="_blank" } aux élèves pour déposer le travail
    - Pour ce même dossier on peut générer un [lien différent à communiquer](https://nuage03.apps.education.fr/index.php/s/eTHmJeHLaNJyR9S){:target="_blank" } à un collègue pour avoir accès aux travaux.
    - Essayer de téléverser un fichier comme un élève, et cliquer sur le lien "vision prof" pour vérifier qu'il a bien été ajouté. Vous pouvez le modifier et le supprimer.


!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant. Voir ce [tutoriel](https://nuage03.apps.education.fr/index.php/s/DdPsmgzxtnABa3A){:target="_blank" } pour créer son compte Apps Education


!!! tip "Prise en main"
    - Lien vers l’outil : [https://portail.apps.education.fr/](https://portail.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Nuage](https://nuage03.apps.education.fr/index.php/s/LfwR6rkTAQ94m95){:target="_blank" }


### :three: Visio-Agents : Faire des visio-conférences
**Visio Agent** est une solution de visioconférence basée sur BBB (BigBlueButton) et hébergée par l’Etat (conforme au RGPD). Elle permet de créer ou non une salle d’attente, de valider ou non certains participants sans que ces derniers n’aient besoin de créer un compte.

!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant. Voir ce [tutoriel](https://nuage03.apps.education.fr/index.php/s/DdPsmgzxtnABa3A){:target="_blank" } pour créer son compte Apps Education

!!! tip "Prise en main"
    - Lien vers l’outil : [https://portail.apps.education.fr/](https://portail.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Démarrer avec Visio Agents](https://ww2.ac-poitiers.fr/dane/spip.php?article947){:target="_blank" }

    
### :four: France Transfert : Transférer des fichiers volumineux
**France transfert** est un nouveau service créé par l’État qui permet d'envoyer des fichiers volumineux non sensibles de manière sécurisée à un agent de l'État ou entre agents. Cette nouvelle application vient en complément de FileSender (RENATER) qui permet de partager des fichiers volumineux de plusieurs giga-octets de façon sécurisée.

France transfert permet d’envoyer jusqu’à 20 Go de fichiers et dossiers par transfert avec une limite de 2 Go maximum par fichier. 


!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant. Voir ce [tutoriel](https://nuage03.apps.education.fr/index.php/s/DdPsmgzxtnABa3A){:target="_blank" } pour créer son compte Apps Education
    
!!! tip "Prise en main"
    - Lien vers l’outil : [https://portail.apps.education.fr/](https://portail.apps.education.fr/){:target="_blank" }
    


