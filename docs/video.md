
# Des outils pour la vidéo :movie_camera: 

### :one: Pod Educ : Enregistrer son écran
Pour utiliser **Pod Educ**, il faut activer son compte _Apps-Education_ (service d’applications gratuit et hébergé par le ministère de l’Education nationale). 
!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.
Voir le [tutoriel](https://nuage03.apps.education.fr/index.php/s/LREbY3BcFt4QLej){:target="_blank" } pour accéder à Apps – Education

!!! tip "Prise en main"
    - Lien vers l’outil : [https://podeduc.apps.education.fr/](https://podeduc.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Enregistrer son écran](https://podeduc.apps.education.fr/video/0128-le-nouvel-enregistreur/){:target="_blank" }
    - Une page avec [différents tutoriels](https://podeduc.apps.education.fr/tutoriels-pod-educ/){:target="_blank" } pour présenter les différentes fonctionnalités de Pod Educ



### :two: Publier une vidéo sur une plafeforme (streaming vidéo)
Deux alternatives à YouTube de Google existent. Elles présentent l'avantage d'héberger les vidéos en France sur des serveurs du le ministère de l'éductaion nationnale
!!! danger "Droit à l'image"
    - Même si ces plateformes sont hébergées en France et sont institutionnelles, elles ne dispensent de faire une demande d'autorisation de prise de vue et de publication (droit à l'image) auprès de l'élève et de ses parents s'il est mineur.
    - Il convient de bien choisir les paramètres de diffusion et de supprimer les contenus dans lesquels les élèves apparaissent au bout d'un an (droit à l'oubli)
Voir le [tutoriel](https://nuage03.apps.education.fr/index.php/s/LREbY3BcFt4QLej){:target="_blank" } pour accéder à Apps – Education

#### :arrow_forward: Portails Tubes
Ce service est destiné à héberger des vidéos que vous avez créées (capsules, production élèves, etc.) sans les héberger sur une plateforme tierce.
Une fois la vidéo en ligne, on peut rajouter des sous-titres, générer un QR-code, créer un lien de partage (avec des droits d’accès), etc.

!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.

!!! tip "Prise en main"
    - Fonctionnement du portail Tubes : [voir la vidéo](https://tube-numerique-educatif.apps.education.fr/w/koKWnHzNAqntRxG3wdvLyV){:target="_blank" }
    - Exemple de vidéo hébergée sur Tubes :  [présentation de apps education](https://tube-numerique-educatif.apps.education.fr/w/ekMHNQk3t5ShDheocbwxsV){:target="_blank" }
  
#### :arrow_forward: Pod Educ
Ce service est plutôt destiné à héberger des vidéos entre collègues et institution (magistère, Visio agents, etc.)
!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://podeduc.apps.education.fr/](https://podeduc.apps.education.fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Publier une vidéo](https://podeduc.apps.education.fr/video/0052-ajouter-une-video/){:target="_blank" }
    - Une page avec [différents tutoriels](https://podeduc.apps.education.fr/tutoriels-pod-educ/){:target="_blank" } pour présenter les différentes fonctionnalités de Pod Educ


### :three: Digiview : Partager et visionner une vidéo YouTube sans les publicités
**Digiview** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
Vous avez trouvé une vidéo sur YouTube mais lors de la diffusion, il y a des publicités qui apparaissent au début voire au milieu de la lecture. Il est possible de les supprimer via l’outil Digiview (appartenant au site La [Digitale](https://ladigitale.dev/){:target="_blank" }). Aucune inscription, ni installation ne sont requises, tout se fait en ligne. Il y a possibilité de générer un **QR-code** et un **lien de partage**, de démarrer ou arrêter la vidéo à un moment précis


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digiview/#/](https://ladigitale.dev/digiview/#/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiview](https://ladigitale.dev/digiplay/#/v/634cff9e1cc02){:target="_blank" }
   

### :four: Digicut : Couper une vidéo (et aussi un audio)
**Digicut** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
C’est un outil qui permet de d’extraire des morceaux d’une vidéo ou d’un audio directement depuis votre navigateur web sans aucun logiciel à installer. Simple et efficace !!


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digicut/](https://ladigitale.dev/digicut/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digicut](https://ladigitale.dev/digiplay/#/v/634d697c33c9d){:target="_blank" }
   

### :five: Digitranscode : Convertir des fichiers vidéo en ligne
**Digitranscode** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. 
C’est un outil qui permet de convertir des fichiers audio et des vidéos directement depuis votre navigateur web sans aucun logiciel à installer. Ceci se révèle particulièrement intéressant dans le cadre de travaux rendus par les élèves sous forme de vidéo ou d'audio dans des formats peu connus.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digitranscode/](https://ladigitale.dev/digitranscode/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digitranscode](https://ladigitale.dev/digiplay/#/v/634d6a15adfba){:target="_blank" }