# Trousse à outils :hammer:

### :one: Ubisit : Créer, gérer des plans de classe
**Ubisit** est une application libre et gratuite de la plateforme **Forge des communs numériques éducatifs** créée par Soren Starck. Il permet de créer un très rapidement, et sans installation, un plan de classe, de tirer au sort des élèves.

Il est possible de faire ses "screens" à l'avance, de les enregistrer sur un support (clé USB, espace de travail) et de les ouvrir par la suite.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Ubisit](https://tube-numerique-educatif.apps.education.fr/w/3nzhE61Rn1TaX9W1vGxv6T){:target="_blank" }
    
### :two: Bitwarden : Coffre fort numérique
**Bitwarden** est un logiciel de sauvegarde de mots de passe (coffre-fort numérique). Lors de l’installation de ce logiciel, il faut définir un mot de passe maître qui sera le seul à retenir. Ensuite vous pouvez y enregistrer tous vos mots de passe qui seront stockés dans un fichier crypté. (Théoriquement il faut un mot de passe fort et unique pour chaque site qui demande une authentification). Ce logiciel peut également permettre de **générer des mots de passe forts** et de les enregistrer automatiquement. 

Vous pouvez l’installer sur vos différents périphériques (tablettes, smartphones et ordinateurs) et ainsi avoir accès à tous vos mots de passe synchronisés sur vos appareils. 


!!! info "Remarque"
    Une inscription est nécessaire. Vous pouvez avoir accès à vos mots de passe sur votre mobile ou sur n'importe quel appareil en vous connectant au site Bitwarden.
    Une version payante est disponible mais la version gratuite est largement suffisante.

!!! danger "Sécurité"
    Le mot de passe que vous définirez pour accéder à Bitwarden doit être fort pour que cette solution soit efficace. Il faudra veiller à ne pas l'oublier car il est difficile de le récupérer.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://bitwarden.com/](https://bitwarden.com/){:target="_blank" }
    - Lien vers un tutoriel : [Installer et utiliser Bitwarden](https://lecrabeinfo.net/utiliser-bitwarden-gestionnaire-mots-de-passe-gratuit-et-open-source.html){:target="_blank" }

    
### :three: SyncBackFree : Sauvegarde et synchronisation de fichiers
Vous avez créé un fichier sur votre PC personnel et l’avez mis sur votre clé USB pour l’utiliser en classe. Mais il y a une erreur et vous modifiez le fichier sur la clé. Mais le fichier qui est sur votre PC n’est pas modifié, vous avez deux versions différentes du même document. Il serait intéressant qu’en revenant chez vous, votre clé et votre PC se synchronisent pour ne garder que la version corrigée du fichier. Ceci est possible avec **SyncBackFree** (version gratuite). 
Vous pouvez soit créer des sauvegardes sur une clé ou un autre dossier (qui peut être un Drive) ou les synchroniser. Il est même possible de configurer le PC pour la synchronisation débute automatiquement quand on branche la clé USB.



!!! info "Remarque"
    Une version payante est disponible mais la version gratuite est largement suffisante.


!!! tip "Prise en main"
    - Lien vers l’outil (version gratuite) : [https://www.2brightsparks.com/download-syncbackfree.html](https://www.2brightsparks.com/download-syncbackfree.html){:target="_blank" }
    - Lien vers un tutoriel :  [Tutoriel succint](https://ladigitale.dev/digiplay/#/v/6357b7879920d){:target="_blank" } -  [Tutoriel complet](https://sospc.name/syncback-free/){:target="_blank" }


### :four: PDF4Teachers : Annoter et corriger des exercices PDF
C’est un logiciel présent sur Linux, Mac et Windows, qui permet de convertir les fichiers image envoyés par les élèves en PDF et de les annoter. Il garde en mémoire le barème et les annotations pour les appliquer plus rapidement. Il calcule automatiquement la note

!!! info "Remarque"
    Une version payante est disponible mais la version gratuite est largement suffisante.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://pdf4teachers.org/](https://pdf4teachers.org/){:target="_blank" }
    - Lien vers un tutoriel :  [Utiliser Pdf4Teachers](https://ladigitale.dev/digiplay/#/v/63541d6f53e35){:target="_blank" }
    - [Documentation](https://pdf4teachers.org/Documentation/){:target="_blank" } de l’éditeur avec les différentes fonctionnalités
