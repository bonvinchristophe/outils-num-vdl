# Des outils pour travailler l'orientation  :construction_worker:

### :one: Folios : Porte document numérique
**Folios** est une application accessible par les enseignants et les élèves depuis l’ENT. Il permet aux élèves de stocker, classer et conserver des documents, des formulaires en lien avec l’orientation de la 6ème à la Terminale

!!! info "Remarque"
    L'accès à folios se fait via l'ENT. Il suffit de cliquer sur le connecteur. 
   

!!! tip "Prise en main"
    - Lien vers le tutoriel : [Documentation](https://view.genial.ly/63334e50c7b5620018b7e641){:target="_blank" }
    
!!! danger "**Fermeture de FOLIOS**"
    - Folios n'est plus mis à jour et sera remplacé à partir de Décembre 2024 par un autre outil **Avenir(s)**
    - Il est possible de récupérer les informations sur Folios jusqu'au 31/12/2024
    - Plus d'informations dans le [document joint](https://folios.onisep.fr/servlet/com.univ.collaboratif.utils.LectureFichiergw?ID_FICHIER=8607685){:target="_blank" } et sur [https://avenirs.onisep.fr/](https://avenirs.onisep.fr/){:target="_blank" }


### :two: Site ENT : Centralisation des ressources
Le site du lycée possède une rubrique **« Orientation »** en page d’accueil où sont regroupées différentes informations actualisées sur l’orientation : portes ouvertes, salons, parcours sup, immersion etc

!!! tip "Prise en main"
    - Lien vers la ressource : [https://lpo-du-val-de-lys-estaires.59.ac-lille.fr/lorientation/](https://lpo-du-val-de-lys-estaires.59.ac-lille.fr/lorientation/){:target="_blank" }


### :three: Digipad : Centraliser et catégoriser les informations
**Digipad** permet de créer des murs avec différentes informations. Les tutoriels pour la prise en main de l'outil sont disponibles  [à cette adresse](https://bonvinchristophe.forge.apps.education.fr/outils-num-vdl/apprendre/#digipad-creer-des-pads-alternative-a-padlet){:target="_blank" }


!!! tip "Prise en main"
    - Exemple de DigiPad créé pour [l'AP en terminale générale  :](https://digipad.app/p/849023/ec5e7693cd159){:target="_blank" }


