# Des outils pour apprendre, mettre des ressources à disposition :unamused: 

### :one: DigiFlashcard : Faire des cartes pour mémoriser
**Digiflashcards** est une application libre et gratuite de la plateforme **Ladigitale.dev.**

Cette application permet de créer facilement et gratuitement des paquets de cartes de "mémorisation active" sous forme de questions / réponses, éventuellement enrichies d’illustrations ("indice de récupération" pour la mémoire) et d’enregistrements audio courts. 

Un mode quiz automatique permet également à l’élève de se tester. 

DIGIFLASCARDS fonctionne très bien sur ordinateur, tablette et smartphone. Les paquets de cartes sont faciles à transmettre aux élèves par lien, QR Code ou lien d’intégration iframe. Un outil, pratique et respectueux des données des utilisateurs.

!!! danger "Accéder à ses flashcards"
    Pour accéder ultérieurement à vos flashcards, il faut mémoriser son adresse en l’ajoutant par exemple dans vos favoris. Le mot de passe créé au début de la création des flashcards sera nécessaire pour une modification ultérieure, pensez à le noter.

!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digiflashcards/](https://ladigitale.dev/digiflashcards/#/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digiflashcards](https://ladigitale.dev/digiplay/#/v/6356974bd01c6){:target="_blank" }
    - Exemple de flashcards : [Exemple en SNT](https://ladigitale.dev/digiflashcards/#/f/630e2ae2c1154){:target="_blank" }
    
!!! info "Remarque"
    D'autres outils existent comme Anki, Quizlet, etc. Mais Digiflashcards est entièrement libre et gratuit sans aucune restriction. Un comparatif d'autres outils est disponible [ici](https://ciel.unige.ch/2016/10/choisir-le-bon-outil-pour-creer-ses-flashcards/){:target="_blank" }

### :two: Digipad : Créer des pads (alternative à Padlet)

**Digipad** est une application libre et gratuite de la plateforme Ladigitale.dev. 

C’est est un mur collaboratif qui ressemble à Padlet mais qui est gratuit, libre et très complet : dépôts de fichiers (25 Mo par fichier, pas de limite en nombre de dépôts), mode écriture ou simple lecture, intégration de liens (sites, vidéos...), partage facile, pas de création de compte par les élèves, pas de restriction en nombre de pads créés... 

Contrairement à Padlet, il n'y a pas de limite de nombre de mur. Il est possible de collaborer avec des collègues.


!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://digipad.app/](https://digipad.app/){:target="_blank" }
    - Lien vers des tutoriels :
        - [Présentation (version courte)](https://ladigitale.dev/digiplay/#/v/635694740db45){:target="_blank" }
        - [Présentation (version complète)](https://ladigitale.dev/digiplay/#/v/6356949aef852){:target="_blank" }
        - [Nouvelles options](https://ladigitale.dev/digiplay/#/v/635694be010e9){:target="_blank" }
    - Lien vers des exemples de pad :
        - [Ressources pour réviser des chapitres en terminale spécialité](https://digipad.app/p/197583/13005cb88ebd8){:target="_blank" }
        - [Présentation des éprueves du bac](https://digipad.app/p/223967/56d05ca1f5d41){:target="_blank" }
        - [Plan de travail en SNT](https://digipad.app/p/469303/a241b1e8d9707){:target="_blank" }



### :three: Digibunch : créer un bouquet de liens
**Digibunch** est une application libre et gratuite de la plateforme Ladigitale.dev. 

Ce module, qui ne nécessite pas de création de compte, permet de créer des pages de liens que vous pouvez ensuite partager. A la création, on vous demande de choisir une question avec une réponse que vous êtes le seul à connaitre, ce qui vous permettra de reprendre la main sur votre DIGIBUNCH pour le modifier.

!!! danger "Accéder à ses bouquets de liens"
    Pour accéder ultérieurement à votre bouquet de lien, il faut mémoriser son adresse en l’ajoutant par exemple dans vos favoris. Le mot de passe créé au début de la création du bouquet de lien sera nécessaire pour une modification ultérieure, pensez à le noter.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digibunch/#/](https://ladigitale.dev/digibunch/#/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digibunch](https://ladigitale.dev/digiplay/#/v/6357d272d7167){:target="_blank" }
    - Un exemple d'utilisation en SNT [sur le thème 1 : le WEB](https://ladigitale.dev/digibunch/#/b/62bd622c73882){:target="_blank" }
       
       
### :four: Digimindmap : Créer une carte mentale
**Digimidmap** est une application libre et gratuite de la plateforme Ladigitale.dev (créée par Emmanuel Zimmert) qui permet de créer des cartes heuristiques ou cartes mentales.

Ses utilisations sont variées : la prise de note ; la préparation d'un exposé ; le remue-méninge ; l'aide au résumé ; la structuration d'un projet ; la révision à partir de mots clés ; l'organisation d'idées ; etc. Digimindmap propose de créer des cartes heuristiques simples, composées de texte. Les productions peuvent être enregistrées et partagées en ligne ou exportées en image (capture d'écran).

L'outil n'a pas été conçu pour réaliser des cartes complexes, mais plutôt pour créer simplement et rapidement un document dans le cadre d'une activité en présence ou à distance. Chaque item bénéficie d'un champ de notes, ce qui permet d'ajouter des informations complémentaires.

!!! danger "Accéder à ses bouquets de liens"
    Pour accéder ultérieurement à votre bouquet de lien, il faut mémoriser son adresse en l’ajoutant par exemple dans vos favoris. Le mot de passe créé au début de la création du bouquet de lien sera nécessaire pour une modification ultérieure, pensez à le noter.


!!! tip "Prise en main"
    - Lien vers l’outil : [https://ladigitale.dev/digimindmap/#/](https://ladigitale.dev/digimindmap/#/){:target="_blank" }
    - Lien vers le tutoriel : [Utiliser Digimindmap](https://ladigitale.dev/digiplay/#/v/6356c5019a773){:target="_blank" }
    - D'autres logiciels et sites plus complets existent pour créer des cartes mentales comme FreeMind, X-Mind, Framindmap

!!! info "Remarque"
    Une application **carte mentale** existe dans l'ENT et permet de partager et de collaborer sur des cartes mentales


### :five: CSE : créer un moteur de recherche personnalisé
**CSE** est une application libre et gratuite créée par Cedric Eyssette (enseignant de philosophie)
Cet outil vous permet de créer un moteur de recherche en limitant l'accès des élèves aux seuls sites que vous aurez autorisés. 
Le problème, quand on apprend aux élèves à travailler avec des moteurs de recherche, est l'exhaustivité des sources qui appparaissent, le tri peut être difficile, notamment pour de jeunes élèves. Avec cet outil, vous mettez quelques sites en référence et la recherche se fera à partir de ces sites. Il est plus facile de travailler avec les élèves sur le tri et la pertinence des informations.


!!! tip "Prise en main"
    - Lien vers le tutoriel : [Utiliser CSE](https://cse.forge.apps.education.fr/){:target="_blank" }
    - Un exemple d'utilisation [en philosophie](https://cse.forge.apps.education.fr/#https://eyssette.forge.apps.education.fr/my-cse/intro-philo.md){:target="_blank" }
    - Un exemple d'utilisation en [SVT](https://cse.forge.apps.education.fr/#https://codimd.apps.education.fr/krUWrUw8QKyQGCz61rKL2A?both){:target="_blank" }

