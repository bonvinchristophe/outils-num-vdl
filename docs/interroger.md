# Des outils pour tester, interroger des élèves en classe :raising_hand:

### :one: Digistorm : Alternative à Kahoot (créer des quiz)
**Digistorm** est une application libre et gratuite de la plateforme **Ladigitale.dev** créée par Emmanuel Zimmert. Elle permet de créer des quizz illustrés à réponses uniques ou multiples, en synchrone ou en asynchrone, sans limite en nombre de quizz ou d’utilisateurs. 

On peut également, avec le même compte, générer d’autres formes d’interaction participative : des sondages, des nuages de mots, des remue-méninges. Une alternative intéressante à Kahoot et autre Quizizz (qui sont limités sour leur version gratuite). 

Autre option intéressante : il est possible d’utiliser DIGISTORM sans création de compte, sur ordinateur comme sur tablette ou smartphone. C’est donc l’outil idéal pour demander à nos élèves de créer rapidement et facilement des quizz, par exemple pour un feedback après un travail de groupe ou une présentation orale...


!!! tip "Prise en main"
    - Lien vers l’outil : [ https://digistorm.app/]( https://digistorm.app/){:target="_blank" }
    - Lien vers le tutoriel vidéo : 
        - [Utiliser Digistorm (sans compte version étudiant)](https://ladigitale.dev/digiplay/#/v/635698d994e23){:target="_blank" }
        - [Utiliser Digistorm (sans compte pour les enseignants))](https://ladigitale.dev/digiplay/#/v/6356990e26a56){:target="_blank" }
        - [Utiliser Digistorm (avec compte pour enseignants)](https://ladigitale.dev/digiplay/#/v/6356993467c1f){:target="_blank" }

### :two: Digibuzzer : Créer un jeu de questions réponses

**Digibuzzer** est une application libre et gratuite de la plateforme Ladigitale.dev créée par Emmanuel Zimmert. Il propose la mise en place d'un concept simple de question / réponse autour d'un buzzer connecté. Lors de la connexion à la salle de jeu, les élèves indiquent un nom ou un pseudo et peuvent sélectionner ou téléverser un avatar (utilisation de Digiface recommandée).

L'enseignant anime ensuite la partie, sur le principe d'un Question pour un champion en présence ou à distance. Les questions sont communiquées à l'oral et l'apprenant le plus rapide peut proposer sa réponse, que l'enseignant valide ou refuse. L'attribution de points et le classement sont directement gérés dans l'application.

**Note : une salle de jeu est accessible en administration pendant une semaine (durée de la session utilisateur), uniquement sur l'appareil et le navigateur utilisé lors de la création de la salle.**


!!! tip "Prise en main"
    - Lien vers l’outil : [https://digibuzzer.app/](https://digibuzzer.app/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Utiliser Digibuzzer](https://ladigitale.dev/digiplay/#/v/6356c621121bd){:target="_blank" }


### :three: Wooclap, créer des quizz
**Wooclap** est une plateforme interactive conçue pour améliorer l'engagement des élèves lors de cours. Elle permet aux utilisateurs de poser des questions, de réaliser des sondages et d'interagir en temps réel.  
Voici quelques fonctionnalités disponibles : Sondages et QCM, Questions ouvertes, Quiz intéractifs, visualisation des résultats, etc.

!!! info "Remarque"
    Une inscription est nécessaire pour l'enseignant.
    
    Wooclap est un outil payant mais qui est gratuit pour les enseignants. Pour cela, l'inscription doit se faire avec votre adresse mail académique.

!!! tip "Prise en main"
    - Lien vers l’outil : [Wooclap](https://www.wooclap.com/fr/){:target="_blank" }
    - Lien vers le tutoriel vidéo : [Débuter avec Wooclap](https://ladigitale.dev/digiview/#/v/6713f17388a09){:target="_blank" }

